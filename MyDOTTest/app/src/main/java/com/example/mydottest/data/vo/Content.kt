package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Content(
    val content: String,
    val id: Int,
    val image: String,
    val media: List<String>,
    val title: String,
    val type: String
)