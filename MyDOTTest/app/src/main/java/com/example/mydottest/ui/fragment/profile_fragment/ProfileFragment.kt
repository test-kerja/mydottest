package com.example.mydottest.ui.fragment.profile_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.mydottest.R
import com.example.mydottest.data.api.ApiClient
import com.example.mydottest.data.api.ApiInterface
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.vo.Profile
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var profileRepository: ProfileRepository

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val endPoin = "user.json"

        val apiService : ApiInterface = ApiClient.getClient()
        profileRepository = ProfileRepository(apiService)

        viewModel = getViewModel(endPoin)

        viewModel.profile.observe(viewLifecycleOwner, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE

        })

    }

    fun bindUI(it: Profile){
        text_notifications.text = it.data.fullname
        nick_name.text = it.data.username
        email.text = it.data.email
        no_telp.text = it.data.phone

        val circularProgressDrawable = CircularProgressDrawable(requireContext())
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(this)
            .load(it.data.avatar)
            .placeholder(circularProgressDrawable)
            .into(profile_image);
    }

    private fun getViewModel(endPoin:String): ProfileViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return ProfileViewModel(profileRepository,endPoin) as T
            }
        })[ProfileViewModel::class.java]
    }
}
