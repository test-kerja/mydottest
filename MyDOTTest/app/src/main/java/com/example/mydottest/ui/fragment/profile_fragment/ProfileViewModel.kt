package com.example.mydottest.ui.fragment.profile_fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.vo.Profile
import io.reactivex.disposables.CompositeDisposable

class ProfileViewModel (private val profileRepository : ProfileRepository, endPoin: String): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val profile : LiveData<Profile> by lazy {
        profileRepository.fetchProfile(compositeDisposable,endPoin)
    }

    val networkState : LiveData<NetworkState> by lazy {
        profileRepository.getProfilesNetworkState()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}