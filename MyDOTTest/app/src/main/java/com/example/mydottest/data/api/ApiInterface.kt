package com.example.mydottest.data.api

import com.example.mydottest.data.vo.Gallery
import com.example.mydottest.data.vo.Profile
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {


    @GET("{id}")
    fun getProfile(@Path("id")id: String): Single<Profile>
    @GET("{id}")
    fun getList(@Path("id")id: String): Single<Profile>
    @GET("{id}")
    fun getGallery(@Path("id")id: String): Single<Gallery>
}