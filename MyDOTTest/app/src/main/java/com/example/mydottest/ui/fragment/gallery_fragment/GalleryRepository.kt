package com.example.mydottest.ui.fragment.gallery_fragment

import androidx.lifecycle.LiveData
import com.example.mydottest.data.api.ApiInterface
import com.example.mydottest.data.repository.NetwokDataSource
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.vo.Gallery
import io.reactivex.disposables.CompositeDisposable

class GalleryRepository  (private val apiService : ApiInterface) {
    lateinit var galleryNetworkDataSource: NetwokDataSource

    fun fetchGallery (compositeDisposable: CompositeDisposable, endPoin: String) : LiveData<Gallery> {
        galleryNetworkDataSource = NetwokDataSource(apiService,compositeDisposable)
        galleryNetworkDataSource.fetchGallery(endPoin)

        return galleryNetworkDataSource.galleryResponse
    }

    fun getGalleryNetworkState(): LiveData<NetworkState> {
        return galleryNetworkDataSource.newtworkState
    }
}