package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Header(
    val subtitle: String,
    val title: String
)