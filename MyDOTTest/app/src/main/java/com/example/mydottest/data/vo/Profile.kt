package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Profile(
    val `data`: Data,
    val message: String,
    @SerializedName("status_code")
    val statusCode: Int
)