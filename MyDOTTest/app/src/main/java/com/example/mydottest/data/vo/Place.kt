package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Place(
    val `data`: DataXX,
    val message: String,
    @SerializedName("status_code")
    val statusCode: Int
)