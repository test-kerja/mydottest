package com.example.mydottest.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mydottest.data.api.ApiInterface
import com.example.mydottest.data.vo.Gallery
import com.example.mydottest.data.vo.Profile
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NetwokDataSource(
    private val apiService: ApiInterface, private val
    compositeDisposable: CompositeDisposable
) {

    private val _networkState = MutableLiveData<NetworkState>()
    val newtworkState: LiveData<NetworkState> get() = _networkState

    private val _profileResponse = MutableLiveData<Profile>()
    val profileResponse: LiveData<Profile>
        get() = _profileResponse

    private val _galleryResponse = MutableLiveData<Gallery>()
    val galleryResponse: LiveData<Gallery>
        get() = _galleryResponse

    fun fetchProfile(endPoin: String) {
        _networkState.postValue(NetworkState.LOADING)
        try {
            compositeDisposable.add(
                apiService.getProfile(endPoin)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _profileResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("ProfileDataSource", it.message.toString())
                        }
                    )
            )
        } catch (e: Exception) {
            Log.e("ProfileDataSource", e.message.toString())
        }
    }

    fun fetchGallery(endPoin: String) {
        _networkState.postValue(NetworkState.LOADING)
        try {
            compositeDisposable.add(
                apiService.getGallery(endPoin)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        {
                            _galleryResponse.postValue(it)
                            _networkState.postValue(NetworkState.LOADED)
                        },
                        {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e("GalleryDataSource", it.message.toString())
                        }
                    )
            )
        } catch (e: Exception) {
            Log.e("GalleryDataSource", e.message.toString())
        }
    }


}