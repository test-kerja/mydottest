package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Data(
    val avatar: String,
    val email: String,
    val fullname: String,
    val id: Int,
    val phone: String,
    val username: String
)