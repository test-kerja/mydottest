package com.example.mydottest.ui.fragment.gallery_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mydottest.R
import com.example.mydottest.data.api.ApiClient
import com.example.mydottest.data.api.ApiInterface
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.vo.Gallery
import com.example.mydottest.ui.adapter.ImageListAdapter
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.fragment_profile.progress_bar
import kotlinx.android.synthetic.main.fragment_profile.txt_error

class GalleryFragment : Fragment() {

    private lateinit var viewModel: GalleryViewModel
    private lateinit var galleryRepository: GalleryRepository

    private val itemList: Array<String>
        get() = arrayOf("Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10", "Item 11", "Item 12", "Item 13", "Item 14", "Item 15", "Item 16", "Item 17", "Item 18", "Item 19", "Item 20", "Item 21", "Item 22")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
//        val gridview = findViewById<GridView>(R.id.gridview)
//
//        val adapter = ImageListAdapter(this, R.layout.list_item, itemList)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val endPoin = "gallery.json"

        val apiService : ApiInterface = ApiClient.getClient()
        galleryRepository = GalleryRepository(apiService)

        viewModel = getViewModel(endPoin)

        viewModel.gallery.observe(viewLifecycleOwner, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE

        })


    }
    fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    fun bindUI(it: Gallery){
//        text_notifications.text = it.data.fullname
        val adapter = ImageListAdapter(requireContext(), R.layout.card_view_item_grid, it.data)
//        gridview.adapter = ImageAdapter(requireContext())
        gridview.adapter = adapter
        gridview.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->

            context?.toast("Ampung bang jago")

        }



    }
    private fun getViewModel(endPoin:String): GalleryViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return GalleryViewModel(galleryRepository,endPoin) as T
            }
        })[GalleryViewModel::class.java]
    }
}
