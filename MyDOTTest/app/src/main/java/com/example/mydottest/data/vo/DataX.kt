package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class DataX(
    val caption: String,
    val image: String,
    val thumbnail: String
)