package com.example.mydottest.ui.fragment.gallery_fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.vo.Gallery
import io.reactivex.disposables.CompositeDisposable

class GalleryViewModel (private val galleryRepository : GalleryRepository, endPoin: String): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val gallery : LiveData<Gallery> by lazy {
        galleryRepository.fetchGallery(compositeDisposable,endPoin)
    }

    val networkState : LiveData<NetworkState> by lazy {
        galleryRepository.getGalleryNetworkState()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}