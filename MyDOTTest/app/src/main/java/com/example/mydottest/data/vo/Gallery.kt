package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class Gallery(
    val `data`: List<DataX>,
    val message: String,
    @SerializedName("status_code")
    val statusCode: Int
)