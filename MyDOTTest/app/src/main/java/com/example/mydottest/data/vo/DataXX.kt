package com.example.mydottest.data.vo


import com.google.gson.annotations.SerializedName

data class DataXX(
    val content: List<Content>,
    val header: Header
)