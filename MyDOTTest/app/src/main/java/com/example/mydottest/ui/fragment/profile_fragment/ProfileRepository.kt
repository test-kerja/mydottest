package com.example.mydottest.ui.fragment.profile_fragment

import androidx.lifecycle.LiveData
import com.example.mydottest.data.api.ApiInterface
import com.example.mydottest.data.repository.NetworkState
import com.example.mydottest.data.repository.NetwokDataSource
import com.example.mydottest.data.vo.Profile
import io.reactivex.disposables.CompositeDisposable

class ProfileRepository (private val apiService : ApiInterface) {
    lateinit var profileNetworkDataSource: NetwokDataSource

    fun fetchProfile (compositeDisposable: CompositeDisposable, endPoin: String) : LiveData<Profile> {
        profileNetworkDataSource = NetwokDataSource(apiService,compositeDisposable)
        profileNetworkDataSource.fetchProfile(endPoin)

        return profileNetworkDataSource.profileResponse
    }

    fun getProfilesNetworkState(): LiveData<NetworkState> {
        return profileNetworkDataSource.newtworkState
    }
}